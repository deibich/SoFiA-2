// ____________________________________________________________________ //
//                                                                      //
// SoFiA 2 (Array_chr.h) - Source Finding Application                   //
// Copyright (C) 2024 The SoFiA 2 Authors                               //
// ____________________________________________________________________ //
//                                                                      //
// Address:  Tobias Westmeier                                           //
//           ICRAR M468                                                 //
//           The University of Western Australia                        //
//           35 Stirling Highway                                        //
//           Crawley WA 6009                                            //
//           Australia                                                  //
//                                                                      //
// E-mail:   tobias.westmeier [at] uwa.edu.au                           //
// ____________________________________________________________________ //
//                                                                      //
// This program is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by //
// the Free Software Foundation, either version 3 of the License, or    //
// (at your option) any later version.                                  //
//                                                                      //
// This program is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         //
// GNU General Public License for more details.                         //
//                                                                      //
// You should have received a copy of the GNU General Public License    //
// along with this program. If not, see http://www.gnu.org/licenses/.   //
// ____________________________________________________________________ //
//                                                                      //

/// @file   Array_chr.h
/// @author Tobias Westmeier
/// @date   10/12/2021
/// @brief  Container class template for storing a dynamic array of values of type `char` (header).


// WARNING: This is a template that needs to be instantiated before use.
//          Do not edit template instances, as they are auto-generated
//          and will be overwritten during instantiation!


#ifndef ARRAY_chr_H
#define ARRAY_chr_H

#include "common.h"


// ----------------------------------------------------------------- //
// Class 'Array_chr'                                                 //
// ----------------------------------------------------------------- //
// The purpose of this class is to provide a convenient way to store //
// multiple values of a specific type in an array-like structure. A  //
// new array can either be of a given size and empty (using the      //
// standard constructor) or provided with a list of comma-separated  //
// values that will be stored in the array and used to determine its //
// size (using the alternative constructor).                         //
// ----------------------------------------------------------------- //

typedef CLASS Array_chr Array_chr;

// Constructor and destructor
PUBLIC Array_chr    *Array_chr_new      (const size_t size);
PUBLIC Array_chr    *Array_chr_new_str  (const char *string);
PUBLIC Array_chr    *Array_chr_copy     (const Array_chr *source);
PUBLIC void          Array_chr_delete   (Array_chr *self);

// Public methods
PUBLIC const char *Array_chr_get_ptr  (const Array_chr *self);
PUBLIC size_t        Array_chr_get_size (const Array_chr *self);
PUBLIC Array_chr    *Array_chr_push     (Array_chr *self, const char value);
PUBLIC char        Array_chr_get      (const Array_chr *self, const size_t index);
PUBLIC Array_chr    *Array_chr_append   (Array_chr *self, const size_t number, const char value);
PUBLIC Array_chr    *Array_chr_set      (Array_chr *self, const size_t index, const char value);
PUBLIC Array_chr    *Array_chr_add      (Array_chr *self, const size_t index, const char value);
PUBLIC Array_chr    *Array_chr_mul      (Array_chr *self, const size_t index, const char value);
PUBLIC Array_chr    *Array_chr_cat      (Array_chr *self, const Array_chr *source);
PUBLIC Array_chr    *Array_chr_sort     (Array_chr *self);

// Private methods
PRIVATE Array_chr   *Array_chr_quicksort(Array_chr *self, const size_t low, const size_t high);
PRIVATE Array_chr   *Array_chr_swap     (Array_chr *self, const size_t i, const size_t j);

#endif
