// ____________________________________________________________________ //
//                                                                      //
// SoFiA 2 (WCS_AST.c) - Source Finding Application                     //
// Copyright (C) 2024 The SoFiA 2 Authors                               //
// ____________________________________________________________________ //
//                                                                      //
// Address:  Tobias Westmeier                                           //
//           ICRAR M468                                                 //
//           The University of Western Australia                        //
//           35 Stirling Highway                                        //
//           Crawley WA 6009                                            //
//           Australia                                                  //
//                                                                      //
// E-mail:   tobias.westmeier [at] uwa.edu.au                           //
// ____________________________________________________________________ //
//                                                                      //
// This program is free software: you can redistribute it and/or modify //
// it under the terms of the GNU General Public License as published by //
// the Free Software Foundation, either version 3 of the License, or    //
// (at your option) any later version.                                  //
//                                                                      //
// This program is distributed in the hope that it will be useful,      //
// but WITHOUT ANY WARRANTY; without even the implied warranty of       //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         //
// GNU General Public License for more details.                         //
//                                                                      //
// You should have received a copy of the GNU General Public License    //
// along with this program. If not, see http://www.gnu.org/licenses/.   //
// ____________________________________________________________________ //
//                                                                      //

/// @file   WCS_AST.c
/// @author Tobias Westmeier
/// @author Tom David Eibich
/// @date   08/10/2024
/// @brief  Class for handling and converting world coordinates using AST.


#include <stdlib.h>
#include <string.h>
#include <ast.h>

#include "Header.h"
#include "WCS.h"



/// @brief Class for handling and converting world coordinates.
///
/// The purpose of this class is to provide a support for World
/// Coordinate System (WCS) conversions in the form of a wrapper around
/// the AST library. The class provides methods for setting up WCS
/// information from a FITS file header and converting between pixel
/// coordinates (x, y, z) and world coordinates (longitude, latitude,
/// spectral) in three dimensions.

CLASS WCS
{
	bool valid;             ///< WCS correctly initialised?
	AstFitsChan *fitschan;  ///< AstFitsChan object from AST.
	AstFrameSet *wcsinfo;   ///< Coordinate frame set object from AST.
};



/// @brief Standard constructor
///
/// Standard constructor. Will create a new WCS object and return a
/// pointer to the newly created object. The object will be set up
/// from the information in the FITS header supplied by the user.
/// If initialisation of the WCS information fails, the property
/// `valid` will be set to `false`. Note that the destructor will
/// need to be called explicitly once the object is no longer
/// required to release any memory allocated during the lifetime
/// of the object.
///
/// @param header    String containing the raw header information of
///                  the FITS data cube (not null-terminated).
/// @param n_keys    Number of header keywords.
/// @param n_axes    Number of WCS axes in the data cube.
/// @param dim_axes  Array holding the size of each axis.
///
/// @return Pointer to newly created WCS object.

PUBLIC WCS *WCS_new(const char *header, const int n_keys, const int n_axes, const int *dim_axes)
{
	// Sanity checks
	check_null(header);
	check_null(dim_axes);
	ensure(n_axes, ERR_USER_INPUT, "Failed to set up WCS; FITS header has no WCS axes.");
	
	// Create new WCS object
	WCS *self = (WCS *)memory(MALLOC, 1, sizeof(WCS));
	
	// Initialise properties
	self->valid = false;
	self->fitschan = NULL;
	self->wcsinfo  = NULL;
	
	// Set up WCS object from header information
	WCS_setup(self, header, n_keys, n_axes, dim_axes);
	
	// Return new WCS object
	return self;
}



/// @brief Destructor
///
/// Destructor. Note that the destructor must be called explicitly
/// if the object is no longer required. This will release the
/// memory occupied by the object.
///
/// @param self  Object self-reference.

PUBLIC void WCS_delete(WCS *self)
{
	if(WCS_is_valid(self))
	{
		astAnnul(self->fitschan);
		astAnnul(self->wcsinfo);
	}
	
	free(self);
	
	return;
}



/// @brief Check if WCS information valid
///
/// Public method for checking if the WCS object has been correctly
/// set up and contains valid WCS information. This should be used
/// before any coordinate conversion is attempted to avoid termination
/// of the process with an error message.
///
/// @param self  Object self-reference.
///
/// @return `true` if WCS is valid, `false` otherwise.

PUBLIC bool WCS_is_valid(const WCS *self)
{
	return self != NULL && self->wcsinfo != NULL && self->fitschan != NULL && self->valid;
}



/// @brief Set up WCS information from FITS header
///
/// Private method for setting up the WCS object with information
/// from the supplied FITS header. If extraction of WCS information
/// fails for some reason, the object will be left in an invalid
/// state; the property `valid` will be set to `false` in this case,
/// which can later be checked by calling WCS_is_valid().
///
/// @param self      Object self-reference.
/// @param header    C string containing the raw header information of
///                  the FITS data cube (not null-terminated).
/// @param n_keys    Number of header keywords.
/// @param n_axes    Number of WCS axes in the data cube (currently unused).
/// @param dim_axes  Array holding the size of each axis (currently unused).

PRIVATE void WCS_setup(WCS *self, const char *header, const int n_keys, const int n_axes, const int *dim_axes)
{
	// ALERT: n_axes and dim_axes not being used in AST version!
	//        For compatibility with WCSLIB version they need to be kept.
	//        Checking their values here just to avert compiler warnings.
	if(n_axes && dim_axes){};
	
	self->valid = false;
	self->fitschan = astFitsChan(NULL, NULL, " ");
	// NOTE: Setting last argument to " " instead of "" to avert compiler warning.
	
	// Create null-terminated copy of header string (AST needs that)
	const size_t old_size = n_keys * FITS_HEADER_LINE_SIZE;
	char *header_null_terminated = (char *)memory(MALLOC, old_size + 1, sizeof(char));
	memcpy(header_null_terminated, header, old_size);
	header_null_terminated[old_size] = '\0';
	
	// Initialise AstFitsChan object with header info
	astPutCards(self->fitschan, header_null_terminated);
	
	// Delete null-terminated header string again
	free(header_null_terminated);
	
	// Set up AstFrameSet object
	self->wcsinfo = (AstFrameSet *)astRead(self->fitschan);
	
	if(!astOK)
	{
		show_warning("AST error %d: %s", astStatus);
		astClearStatus;
		return;
	}
	
	if(self->wcsinfo == AST__NULL)
	{
		show_warning("No valid WCS information found.");
		return;
	}
	
	if(strcmp(astGetC(self->wcsinfo, "Class"), "FrameSet"))
	{
		show_warning("Unexpected WCS input.");
		// Something unexpected was read (i.e. not a FrameSet)
		return;
	}
	
	// astShow(self->wcsinfo);
	self->valid = true;
	return;
}



/// @brief Convert from pixel to world coordinates
///
/// Public method for converting the pixel coordinates (`x`, `y`, `z`)
/// to world coordinates (`longitude`, `latitude`, `spectral`). Note
/// that the implicit assumption is made that the first up-to-three axes
/// of the cube are in the aforementioned order. Pixel coordinates
/// must be zero-based; world coordinates will be in degrees (`longitude`
/// and `latitude`) or in the native units of the data cube (`spectral`).
/// `longitude`, `latitude` and `spectral` can be `NULL` in which case
/// they are not updated. If invalid input coordinates are supplied by
/// the user, then a warning message will be printed and the output
/// coordinate variables will be left unchanged.
///
/// @param self       Object self-reference.
/// @param x          x coordinate (0-based).
/// @param y          y coordinate (0-based).
/// @param z          z coordinate (0-based).
/// @param longitude  Pointer for holding longitude coordinate (in degrees).
/// @param latitude   Pointer for holding latitude coordinate (in degrees).
/// @param spectral   Pointer for holding spectral coordinate (in native header units).
///
/// @return Returns 1 if the AST library is being used, 0 otherwise.

PUBLIC int WCS_convertToWorld(const WCS *self, const double x, const double y, const double z, double *longitude, double *latitude, double *spectral)
{
	// Sanity checks
	ensure(WCS_is_valid(self), ERR_USER_INPUT, "Failed to convert coordinates; no valid WCS definition found.");
	
	// Determine number of WCS axes
	const size_t n_axes = astGetI(self->wcsinfo, "Naxes");
	ensure(n_axes, ERR_USER_INPUT, "Failed to convert coordinates; no valid WCS axes found.");
	
	// Input pixel coordinates
	// NOTE: WCS pixel arrays are 1-based, hence need to add 1 here!
	const double x_in = n_axes > 0 ? x + 1.0 : 1.0;
	const double y_in = n_axes > 1 ? y + 1.0 : 1.0;
	const double z_in = n_axes > 2 ? z + 1.0 : 1.0;
	const double *array_in[3] = {&x_in, &y_in, &z_in};
	
	// Output world coordinates
	double longitude_out = 0.0;
	double latitude_out  = 0.0;
	double spectral_out  = 0.0;
	double *array_out[3] = {&longitude_out, &latitude_out, &spectral_out};
	
	// Convert from pixels to WCS
	astTranP8(self->wcsinfo, 1, n_axes > 3 ? 3 : n_axes, &array_in[0], 1, n_axes > 3 ? 3 : n_axes, &array_out[0]);
	
	if(astOK)
	{
		// ALERT: AST may return negative longitudes, hence fix needed!
		while(longitude_out < 0.0) longitude_out += 2.0 * M_PI;
		
		// ALERT: AST ignores WCS units for celestial coordinates and always
		//        returns radians! Explicitly converting to degrees here.
		if(n_axes > 0 && longitude != NULL) *longitude = RAD_TO_DEG * longitude_out;  // rad -> deg
		if(n_axes > 1 && latitude  != NULL) *latitude  = RAD_TO_DEG * latitude_out;   // rad -> deg
		if(n_axes > 2 && spectral  != NULL) *spectral  = spectral_out;
	}
	else
	{
		show_warning("AST error %d: %s", astStatus);
		astClearStatus;
	}
	
	return 1;
}



/// @brief Convert from world to pixel coordinates
///
/// Public method for converting the world coordinates (`longitude`,
/// `latitude`, `spectral`) to pixel coordinates (`x`, `y`, `z`). Note
/// that the implicit assumption is made that the first up-to-three axes
/// of the cube are in the aforementioned order. Pixel coordinates
/// will be zero-based; world coordinates must be in degrees for the
/// `longitude` and `latitude` and in the native units of the data
/// cube for `spectral`. `x`, `y` and `z` can be `NULL`, in which case
/// they are not updated. If invalid input coordinates are supplied
/// by the user, then a warning message will be printed and the
/// output coordinate variables will be left unchanged.
///
/// @param self       Object self-reference.
/// @param longitude  Longitude coordinate (in degrees).
/// @param latitude   Latitude coordinate (in degrees).
/// @param spectral   Spectral coordinate (in native header units).
/// @param x          Pointer for holding x coordinate (0-based).
/// @param y          Pointer for holding y coordinate (0-based).
/// @param z          Pointer for holding z coordinate (0-based).

PUBLIC void WCS_convertToPixel(const WCS *self, const double longitude, const double latitude, const double spectral, double *x, double *y, double *z)
{
	// Sanity checks
	ensure(WCS_is_valid(self), ERR_USER_INPUT, "Failed to convert coordinates; no valid WCS definition found.");
	
	// Determine number of WCS axes
	const size_t n_axes = astGetI(self->wcsinfo, "Naxes");
	ensure(n_axes, ERR_USER_INPUT, "Failed to convert coordinates; no valid WCS axes found.");
	
	// Input WCS coordinates
	const double longitude_in = DEG_TO_RAD * longitude;  // deg -> rad
	const double  latitude_in = DEG_TO_RAD * latitude;   // deg -> rad
	const double  spectral_in = spectral;
	const double *array_in[3] = {&longitude_in, &latitude_in, &spectral_in};
	
	// Output pixel coordinates
	double x_out = 0.0;
	double y_out = 0.0;
	double z_out = 0.0;
	double *array_out[3] = {&x_out, &y_out, &z_out};
	
	// Convert from WCS to pixel
	astTranP8(self->wcsinfo, 1, n_axes > 3 ? 3 : n_axes, &array_in[0], 0, n_axes > 3 ? 3 : n_axes, &array_out[0]);
	
	if(astOK)
	{
		// NOTE: WCS pixel arrays are 1-based, so need to subtract 1 here
		//       to obtain 0-based values!
		if(n_axes > 0 && x != NULL) *x = x_out - 1.0;
		if(n_axes > 1 && y != NULL) *y = y_out - 1.0;
		if(n_axes > 2 && z != NULL) *z = z_out - 1.0;
	}
	else
	{
		show_warning("AST error %d: %s", astStatus);
		astClearStatus;
	}
	
	return;
}
